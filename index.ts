import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetWorkPagingComponent } from './src/base.component';

export * from './src/base.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetWorkPagingComponent
  ],
  exports: [
    BaseWidgetWorkPagingComponent
  ]
})
export class WorkPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WorkPagingModule,
      providers: []
    };
  }
}
