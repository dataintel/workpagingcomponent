import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-paging',
  template: `<style>
  .mdl-data-table {
    table-layout:center;
    width:700px;
    border: 1px;
    position: absolute; 
}
#my-table td, tr {
  width: 500px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -o-text-overflow: ellipsis;
  border: 1px solid black;
}

/* unrelated to responsive table css */
#my-table{
  margin-top:24px;
  border: 1px;
}
.material-icons {
  vertical-align: -25%;
  border: 1px;
}

.row:hover {
    background:#6db3ef !important;
    cursor: pointer ;
    color: #ffffff !important;
}
</style>

<div class="col-md-6" style="padding: 10px">
    <div class="col-md-6" style="margin: 0px 0px 5px 0px" *ngFor="let items of persons">
      <div class="row" style="padding: 10px; border: solid 2px rgba(224, 224, 224, 0.33); margin-right: -10px;">
      
        <h4 >{{items.name}} 
        </h4>
      </div>
      
    </div>  
    
</div>`
})
export class BaseWidgetWorkPagingComponent implements OnInit {

 constructor() { }

  ngOnInit() {
  }
  
  public persons:Array<any> = [
    {"name" : "eBdesk Malaysia"
    },
    {"name" : "Metro"
    },
    {"name" : "Partner"
    },
    {"name" : "Test23022017-3"
    },
    { "name" :   "General"
    },
    { "name" : "NLP eBdesk"
    },
    {"name" : "Test23022017-2"
    },
    {"name" : "Test23022017-4"
    },
   ]
   
}
