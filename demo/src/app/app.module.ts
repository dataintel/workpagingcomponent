import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule } from 'ng2-bootstrap/ng2-bootstrap';
import { MaterialModule } from '@angular/material';
import { } from '@angular-mdl/core';

import { AppComponent } from './app.component';
import { WorkPaging } from '../../../index';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule,
    MaterialModule,
    WorkPaging
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
